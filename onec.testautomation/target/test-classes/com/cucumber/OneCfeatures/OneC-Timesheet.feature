#Business Use Case : OneC - Timesheet
#Total Scenarios in Test : 1
#Author: viduthalaivirumbi.kanagaraj@cignizant.com
##Comments : Initial Health Check Test scenarios
#************************************************************************************************************************************#
@OneC
Feature: OneC timesheet navigation
#************************************************************************************************************************************#
#************************************************************************************************************************************#
#Test Scenario Number :001
@Timesheet @Navigation
 Scenario: Validate time sheet page navigation
    Given user launches OneC application
    When user navigates to Timesheet application
    And close OneC