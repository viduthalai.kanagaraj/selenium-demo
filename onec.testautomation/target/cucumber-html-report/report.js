$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("OneC-Timesheet.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Business Use Case : OneC - Timesheet"
    },
    {
      "line": 2,
      "value": "#Total Scenarios in Test : 1"
    },
    {
      "line": 3,
      "value": "#Author: viduthalaivirumbi.kanagaraj@cignizant.com"
    },
    {
      "line": 4,
      "value": "##Comments : Initial Health Check Test scenarios"
    },
    {
      "line": 5,
      "value": "#************************************************************************************************************************************#"
    }
  ],
  "line": 7,
  "name": "OneC timesheet navigation",
  "description": "",
  "id": "onec-timesheet-navigation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 6,
      "name": "@OneC"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 8,
      "value": "#************************************************************************************************************************************#"
    },
    {
      "line": 9,
      "value": "#************************************************************************************************************************************#"
    },
    {
      "line": 10,
      "value": "#Test Scenario Number :001"
    }
  ],
  "line": 12,
  "name": "Validate time sheet page navigation",
  "description": "",
  "id": "onec-timesheet-navigation;validate-time-sheet-page-navigation",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Timesheet"
    },
    {
      "line": 11,
      "name": "@Navigation"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "user launches OneC application",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "user navigates to Timesheet application",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "close OneC",
  "keyword": "And "
});
formatter.match({
  "location": "OneC_001_Common.user_launches_OneC_application()"
});
formatter.result({
  "duration": 43982482698,
  "status": "passed"
});
formatter.match({
  "location": "OneC_002_Timesheet.user_navigates_to_Timesheet_application()"
});
formatter.result({
  "duration": 16783657195,
  "status": "passed"
});
formatter.match({
  "location": "OneC_001_Common.close_OneC()"
});
formatter.result({
  "duration": 1479320606,
  "status": "passed"
});
});