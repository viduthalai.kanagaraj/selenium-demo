package com.cucumber.selenium;
//import java.io.File;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.hamcrest.CoreMatchers;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.deps.com.thoughtworks.xstream.core.util.Base64Encoder;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.cucumber.selenium.Stringencryptdecrypter;

public class Commanseleniumfunctions {
	
	public static WebDriver driver;
	public static WebDriverWait waitVar = null;
	
	public void chromelogin(String userid,String password,String baseURL)throws Exception {
	    /*
		//use for SSO applications/windows crecentials app
		String username = "";
	    userid = "username";
	    password = "encrypted password";
		Stringencryptdecrypter td= new Stringencryptdecrypter();
		if (userid== null){
			Assert.fail("Please retry providing valid app userid");
		};
		if (password== null){
			Assert.fail("Please retry providing valid app password");
		};*/
		/*
		String Decryptedpassword = td.decrypt(password);
		if (userid.trim().toUpperCase().contains("SILVER")) {
		    username = userid.trim().replace("\\", "%5C");
		}
		else if (userid.trim().toUpperCase().contains("INTERNAL")) {
		    username = userid.trim().replace("\\", "%5C");
		}
		else {
		    username = "INTERNAL%5C"+ userid.trim();
		}
		*/
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();        
		options.addArguments("chrome.switches", "--disable-web-security --disable-extensions --disable-extensions-file-access-check --disable-extensions-http-throttling --disable-infobars --enable-automation --start-maximized");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		prefs.put("profile.default_content_settings.popups", 0);
		options.setExperimentalOption("prefs", prefs);
		new DesiredCapabilities();
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		caps.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(caps);
		waitimplicit(1);
		System.out.println("Running test as user:-"+ userid);
		/*
		//use for SSO applications/windows crecentials app
		driver.get("https://"+username+":"+Decryptedpassword+"@"+baseURL);
		*/
		driver.navigate().to("https:"+baseURL);
		waitimplicit(6);
	}
	
	public void waitimplicit(int wtime) throws InterruptedException {
		int waittime = wtime * 1000;
		Thread.sleep(waittime);
	}
	
	public void waituntiltitlecontains(int wtime,String wstring) {
		 WebDriverWait wait = new WebDriverWait(driver, wtime);
		 wait.until(ExpectedConditions.titleContains(wstring));
	}
	
	public void teardownOneC() {
		driver.quit();
	}
	
	public void println(String msg) {
		System.out.println(msg);
	}
	
	public static String getmystring(String mystring) throws Exception {
		Stringencryptdecrypter td= new Stringencryptdecrypter();
		String encryptedpassword = td.encrypt(mystring);
		return encryptedpassword;
	}
	public int randonnumbergeneration(){
		Random recordId1 = new Random();
		int  i = recordId1.nextInt(550) + 1;
		return i;}
	
	public String randomnamegeneration(){
		Random recordname1 = new Random();
		String a= recordname1.toString() +1;
		return a;
	}
	public Boolean isdisplayed_xpath(String xpath) {
		Boolean displayed = false;
		try {
			displayed = driver.findElement(By.xpath(xpath)).isDisplayed();
		} catch (Exception e) {}
		return displayed;
	}
	public Boolean wait_isdisplayed_xpath(String xpath,int timeoutsec) {
		Boolean displayed = false;
		for (int d=0; d <= timeoutsec; d++) {
			try {
				if (isdisplayed_xpath(xpath)) {
					displayed = true;
					println(xpath+" displayed within timeout");
				    break;
				} else { waitimplicit(1); }
			} catch (Exception e) {}
		}
		if (!displayed){ println("Error: "+ xpath+" Not displayed within timeout: "+timeoutsec); }
		return displayed;
	}
	public Boolean sendkeys_xpath(String xpath,String value) {
		Boolean sendkeys = true;
		try {
				driver.findElement(By.xpath(xpath)).sendKeys(value);
		} catch (Exception e) {
				 System.out.println(e);
				 sendkeys = false;
		}
		if (!sendkeys){println("Error: "+ xpath+"  Not entered");}
		return sendkeys;
	}
	public Boolean click_xpath(String xpath) {
		Boolean click = true;
		try {
				driver.findElement(By.xpath(xpath)).click();
		} catch (Exception e) {
				System.out.println(e);
				click = false;
		}
		if (!click){println("Error: "+ xpath+"  Not Clicked");}
		return click;
	}
	public Boolean change_iframe(String iframe) {
		Boolean frame = false;
		try {
			if (!iframe.contains("default")) {
				driver.switchTo().frame(iframe);
				frame = true;
				println("Switched to iframe:"+iframe);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return frame;
	}
}
