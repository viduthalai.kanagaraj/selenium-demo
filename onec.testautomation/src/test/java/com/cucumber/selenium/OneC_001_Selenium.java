package com.cucumber.selenium;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class OneC_001_Selenium extends Commanseleniumfunctions { 
static Commanseleniumfunctions Csf = new Commanseleniumfunctions();
		//login to OneC
		public void LogintoOneC() throws Exception {
			//Using chrome browser - to get from environment variables
			//String userid = System.getProperty("userid");
			//String encryptedpassword = System.getProperty("password");
			String username = "123456-username";
			String password = "123456-password";
			String baseURL = "onecognizant.cognizant.com";
			System.out.println("enviroment base URL:-"+ baseURL);
			Csf.chromelogin(username, password,baseURL);
			if (wait_isdisplayed_xpath ("//*[@id='username']",10)){
				Csf.sendkeys_xpath("//*[@id='username']",username);
				Csf.sendkeys_xpath("//*[@id='PasswordInternal']",password);
				Csf.click_xpath("//*[@id='Log_On1']");
				if (!Csf.wait_isdisplayed_xpath ("//*[@id='onec']",10)) {
					Assert.fail("OneC logo - to confirm login success not displayed within 10 sec");
				}
			} else {
				Assert.fail("Expected One C username field not displayed with in time out");
			}
		}
		//close browser
		public void CloseOneC() throws Exception {
			Csf.teardownOneC();
		}
		//close browser
		public Boolean Searchapplication(String application) throws Exception {
			Boolean search=false;
			try {
				Csf.sendkeys_xpath("//*[@id='txtPlatformBarSearch']",application);
				Csf.click_xpath("//*[@id='btnsearch']");
				search=true;
				Csf.waitimplicit(6);
			} catch (Exception e) {}
			return search;
		}
		public Boolean Gototimesheet() throws Exception {
			Boolean timesheet=false;
			String iframe = "appFrame";
			try {
			Csf.change_iframe(iframe);
				if (Csf.wait_isdisplayed_xpath ("//*[@id='desktopsearchresultgeo']/ul/li/div[1]",10)) {
					Csf.click_xpath("//*[@id='desktopsearchresultgeo']/ul/li/div[1]");
					Csf.waitimplicit(30);
					timesheet=true;
				}
			} catch (Exception e) {}
			return timesheet;
		}
}