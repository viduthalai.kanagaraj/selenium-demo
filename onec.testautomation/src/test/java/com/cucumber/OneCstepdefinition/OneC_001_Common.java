package com.cucumber.OneCstepdefinition;
import com.cucumber.selenium.OneC_001_Selenium;
import com.cucumber.selenium.Commanseleniumfunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class OneC_001_Common extends Commanseleniumfunctions {
	static Commanseleniumfunctions Csf = new Commanseleniumfunctions();
	static OneC_001_Selenium OCS001 = new OneC_001_Selenium();

    @Given("^user launches OneC application$")
    public void user_launches_OneC_application() throws Exception {
    	OCS001.LogintoOneC();
        Csf.println("Successful navigation of User to OneC");
    }
    
	@And("^close OneC$")
	public void close_OneC() throws Exception {
		OCS001.CloseOneC();
	    Csf.println("Successfully Closed Browser");
	}
}