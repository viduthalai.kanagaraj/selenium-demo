package com.cucumber.OneCstepdefinition;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cucumber.selenium.Commanseleniumfunctions;
import com.cucumber.selenium.OneC_001_Selenium;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class OneC_002_Timesheet extends Commanseleniumfunctions {
	static Commanseleniumfunctions Csf = new Commanseleniumfunctions();
	static OneC_001_Selenium OCS001 = new OneC_001_Selenium();	
    @Given("^user navigates to Timesheet application$")
    public void user_navigates_to_Timesheet_application() throws Exception {
    	if (OCS001.Searchapplication("Submit TimeSheet")) {
    		if (OCS001.Gototimesheet()) {
    			} else { Assert.fail("Error: Navigation to Time sheet application failed");}
    	} else { Assert.fail("Error: Unable to find Tme sheet application");}
    }
}
