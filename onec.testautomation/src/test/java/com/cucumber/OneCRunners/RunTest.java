package com.cucumber.OneCRunners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

//@RunWith(Cucumber.class)
@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber.json"
       ,retryCount = 0,
       detailedReport = true,
       detailedAggregatedReport = true,
       overviewReport = true,
        //coverageReport = true,
        jsonUsageReport = "target/cucumber-usage.json",
        //usageReport = true,
        toPDF = true,
        excludeCoverageTags = {"@flaky" },
        includeCoverageTags = {"@passed" },
        outputFolder = "target/cucumber-html-report/Resultreports")
@CucumberOptions
			(plugin = { "html:target/cucumber-html-report",
		        "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
		       "junit:target/cucumber-results.xml" },
			features = {"src/test/java/com/cucumber/OneCfeatures"},
			glue = "com.cucumber.OneCstepdefinition"
        ,tags = {"@Timesheet"} 			
        //dryRun = true
        )

public class RunTest {

}
